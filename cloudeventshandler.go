package cloudeventshandler

import (
	"context"
	"encoding/json"
	"time"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	"gitlab.com/sbongini-go/mapstructureplus"
	"gitlab.com/sbongini-go/ratatoskr/core"
	"gitlab.com/sbongini-go/ratatoskr/core/handler"
	"gitlab.com/sbongini-go/ratatoskr/core/model"
)

func NewFromInterface(data interface{}) (core.Handler, error) {
	h := &cloudEventsHandler{}
	err := mapstructureplus.PrivateDecode(data, &h)
	return h, err
}

func (h cloudEventsHandler) MarshalYAML() (interface{}, error) {
	return struct {
		Name        string
		Type        string
		NextHandler string
	}{
		Name:        h.Name(),
		Type:        h.Type(),
		NextHandler: h.nextHandler,
	}, nil
}

type cloudEventsHandler struct {
	// embedded type contente le parti comuni degli handlers
	handler.CommonHandler `mapstructure:",squash"`

	// nextHandler Handler a cui inoltrare l'evento dopo che e' trascorso il ritardo randomico
	nextHandler string `yaml:"nexthandler"`

	serviceName string `yaml:"-"`
}

func New(nextHandler string) *cloudEventsHandler {
	return &cloudEventsHandler{
		nextHandler: nextHandler,
	}
}

// Type restituisce il tipo di handler
func (h *cloudEventsHandler) Type() string {
	return "cloudevents"
}

// Init funzione di inizializzazione del Handler
func (h *cloudEventsHandler) Init(ctx context.Context, initBag core.InitHandlerBag) error {
	h.SetLogger(initBag.Logger)
	h.serviceName = initBag.ServiceName
	return nil
}

// Execute esegue la logica del Handler
func (h cloudEventsHandler) Execute(ctx context.Context, bridge *core.Bridge, inputEvent model.Event) (model.Event, error) {
	// Creo un nuovo oggetto cloud events
	cloudEvent := cloudevents.NewEvent()

	// Imposto il campo Data del CloudEvents
	inputData, err := json.Marshal(inputEvent.Data)
	if nil != err {
		return model.Event{}, err
	}
	if contentType := inputEvent.DataContentType(); contentType == "" {
		cloudEvent.SetData(cloudevents.TextPlain, inputData)
	} else {
		cloudEvent.SetData(contentType, inputData)
	}

	// TODO popolare tutti i campi della specifica CloudEvents
	cloudEvent.SetDataSchema("TODO")    // TODO parametrico o da haeders
	cloudEvent.SetSource(h.serviceName) // TODO deve essere possibile iniettarlo da fuori
	cloudEvent.SetID("TODO")            // TODO parametrico o da haeders
	cloudEvent.SetType("TODO")          // TODO parametrico o da haeders
	cloudEvent.SetTime(time.Now())

	// Effettuo il Marshal del CloudEvent
	data, err := cloudEvent.MarshalJSON()
	if nil != err {
		return model.Event{}, err
	}

	// Genero l'evento di output
	outputEvent := model.Event{
		Data: data,
	}
	outputEvent.SetDataContentType(cloudevents.ApplicationJSON)

	return bridge.Next(ctx, h.nextHandler, outputEvent)
}
